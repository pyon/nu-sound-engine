package sounds;

public class RelativePath {
	private String path;
	
	public RelativePath() {
		this.path = "/";
	}
	
	public RelativePath(String path) {
		this.path = simplifyPath(path);
	}
	
	//loose equality
	public boolean equals(String other) {
		return this.path.equals(simplifyPath(other));
	}
	
	public boolean equals(RelativePath other) {
		return this.path.equals(other.path);
	}
	
	public RelativePath concatenate(String path2) {
		return new RelativePath(this.path + simplifyPath(path2));
	}
	
	public RelativePath concatenate(RelativePath path2) {
		return new RelativePath(this.path + simplifyPath(path2.path));
	}
	
	public String toString() {
		return path;
	}
	
	private static String simplifyPath(String path)
	{
		path = path.replaceAll("\\\\", "/");
		int len = path.length();
		for (int i = 0; i < len; i++)
		{
			if (path.charAt(i) != '/')
			{
				path = path.substring(i, len);
				break;
			}
		}
		
		String[] sub = path.split("/");
		len = sub.length;
		for (int i = 1; i < len; i++)
			if (sub[i].equals(".."))
				for (int j = i-1; j >= 0; j--)
				{
					if (sub[j].equals(".."))
						break;
					if (sub[j].length() != 0)
					{
						sub[i] = "";
						sub[j] = "";
						break;
					}
				}
		
		String out = "";
		for (int i = 0; i < len; i++)
		{
			if (sub[i].length() == 0)
				continue;
			out += "/" + sub[i];
		}
		
		if (out.length() == 0)
			return "/";
		return out;
	}
}
