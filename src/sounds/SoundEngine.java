package sounds;

import java.util.ArrayList;

import javax.sound.sampled.LineEvent;
import javax.sound.sampled.LineListener;

public class SoundEngine {
	
	public static ArrayList<Sound> musics = new ArrayList<Sound>(16);
	public static ArrayList<Sound> sfxs = new ArrayList<Sound>(32);
	
	private static RelativePath musicBaseDir = new RelativePath();
	private static RelativePath sfxBaseDir = new RelativePath();
	
	private static Sound getSound(ArrayList<Sound> list, RelativePath path) {
		for (Sound s : list)
			if (path.equals(s.getPath()))
				return s;
		return null;
	}
	
	public static Sound getMusic(String path) {
		return getSound(musics, new RelativePath(path));
	}
	
	public static Sound getSfx(String path) {
		return getSound(sfxs, new RelativePath(path));
	}
	
	private static Sound initSound(String path) {
		if (isOgg(path))
			return new OggSound(path);
		if (isWav(path))
			return new WavSound(path);
		System.err.println("Sound is not a supported filetype (ogg, wav): " + path);
		return null;
	}
	
	public static Sound loadMusic(String path) {
		RelativePath relPath = musicBaseDir.concatenate(path);
		Sound s = getSound(musics, relPath);
		
		if (s == null) {
			s = initSound(path);
			musics.add(s);
		}
		
		return s;
	}
	
	public static Sound loadSfx(String path) {
		RelativePath relPath = sfxBaseDir.concatenate(path);
		Sound s = initSound(path);
		sfxs.add(s);
		return s;
	}
	
	public static boolean isPlayingMusic() {
		for (Sound s : musics)
			if (!s.isStopped())
				return true;
		return false;
	}
	
	public static boolean isPlayingSfx() {
		for (Sound s : sfxs)
			if (!s.isStopped())
				return true;
		return false;
	}

	public static void playMusic(String path) {
		loadMusic(path).play();
	}

	public static void playSfx(String path) {
		final Sound s = loadSfx(path);
		
		final Sound snd = s;
		s.play();
		snd.addLineListener(new LineListener() {
			public void update(LineEvent event) {
				if (event.getType() == LineEvent.Type.STOP) {
					snd.close();
					SoundEngine.sfxs.remove(this);
				}
			}
		});
	}

	public static void loopMusic(String path) {
		loadMusic(path).loop();
	}

	public static void loopSfx(String path) {
		final Sound s = loadSfx(path);
		
		final Sound snd = s;
		s.loop();
		snd.addLineListener(new LineListener() {
			public void update(LineEvent event) {
				if (event.getType() == LineEvent.Type.STOP) {
					snd.close();
					SoundEngine.sfxs.remove(this);
				}
			}
		});
	}
	
	public static void stopMusic(String path) {
		Sound s = getSound(musics, new RelativePath(path));
		
		if (s == null) {
			System.err.println(path + " was not found");
			return;
		}
		s.stop();
	}

	public static void stopSfx(String path) {
		Sound s = getSound(sfxs, new RelativePath(path));
		
		if (s == null) {
			System.err.println(path + " was not found");
			return;
		}
		s.close();
		sfxs.remove(s);
	}

	public static void stopAllMusic() {
		for (Sound s : sfxs)
			s.close();
	}
	
	public static void stopAllSfxs() {
		for (Sound s : sfxs)
			s.close();
		sfxs.clear();
	}

	public static void stopAll() {
		stopAllMusic();
		stopAllSfxs();
	}

	public static void setMasterVolume(float f) {
		//TODO: this
		return;
	}

	public static boolean isOgg(String sound) {
		return sound.toLowerCase().endsWith(".ogg");
	}

	public static boolean isWav(String sound) {
		return sound.toLowerCase().endsWith(".wav");
	}
}
