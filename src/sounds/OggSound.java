package sounds;

import java.io.IOException;
import java.io.InputStream;

import javax.sound.sampled.LineEvent;
import javax.sound.sampled.LineListener;

import org.newdawn.easyogg.OggClip;

public class OggSound implements Sound {
	private OggClip clip;
	public RelativePath location;

	public OggSound(String sound) {
		location = new RelativePath(sound);
		try {
			InputStream fis = this.getClass().getClassLoader().getResourceAsStream(sound);
			clip = new OggClip(fis);
		} catch (IOException e) {
			e.printStackTrace();
			System.err.println("sound clip not found.");
		}
	}

	@Override
	public void play() {
		clip.play();
	}

	@Override
	public void stop() {
		clip.stop();
	}

	@Override
	public boolean isStopped() {
		return clip.stopped();
	}
	
	public void pause() {
		clip.pause();
	}

	@Override
	public boolean isPaused() {
		return clip.isPaused();
	}

	@Override
	public void resume() {
		clip.resume();
	}

	@Override
	public void loop() {
		clip.loop();
	}
	
	public void loop(int n) {
		clip.loop(n);
	}
	
	public void close() {
		clip.close();
	}

	public boolean isClosed() {
		return clip.isClosed();
	}
	
	@Override
	public void setVolume(float vol) {
		if (vol > 1.0f)
			vol = 1.0f;
		else if (vol < 0.0f)
			vol = 0.0f;
		clip.setVolume(vol);
	}

	@Override
	public String getPath() {
		return location.toString();
	}
	
	public String toString() {
		return "OggSound(" + location + ")";
	}
	
	public void addLineListener(LineListener ll) {
		clip.addLineListener(ll);
	}
}
