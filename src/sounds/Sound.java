package sounds;

import javax.sound.sampled.LineListener;

public interface Sound {
	public void play();

	public void stop();
	public boolean isStopped();

	public void loop();
	public void loop(int n);
	
	public void pause();
	public boolean isPaused();
	
	public void resume();

	public void setVolume(float vol);

	public String getPath();
	public String toString();
	
	public void close();
	public boolean isClosed();
	
	public void addLineListener(LineListener ll);
}
