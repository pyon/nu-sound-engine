package sounds;

import java.io.InputStream;

import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.FloatControl;
import javax.sound.sampled.LineEvent;
import javax.sound.sampled.LineListener;
import javax.sound.sampled.LineUnavailableException;

public class WavSound implements Sound {
	private Clip clip;
	public RelativePath location;

	public WavSound(String sound) {
		location = new RelativePath(sound);
		try {
			clip = AudioSystem.getClip();
			InputStream fis = this.getClass().getClassLoader().getResourceAsStream(sound);
			clip.open(AudioSystem.getAudioInputStream(fis));
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("sound clip not found or had error");
		}
	}

	@Override
	public void play() {
		clip.setFramePosition(0);
		clip.start();
	}

	@Override
	public void stop() {
		clip.setFramePosition(0);
		clip.stop();
	}
	
	@Override
	public boolean isStopped() {
		return !clip.isActive();
	}
	
	public void pause() {
		clip.stop();
	}
	
	public boolean isPaused() {
		return !clip.isRunning();
	}
	
	public void resume() {
		clip.start();
	}

	@Override
	public void loop() {
		clip.loop(Clip.LOOP_CONTINUOUSLY);
	}
	
	public void loop(int n) {
		clip.loop(n);
	}
	
	public void close() {
		clip.close();
	}

	public boolean isClosed() {
		return !clip.isOpen();
	}
	
	@Override
	public void setVolume(float vol) {
		if (vol > 1.0f)
			vol = 1.0f;
		else if (vol < 0.0f)
			vol = 0.0f;
		
		float gainDB = (float) (20.0 * Math.log(vol));
		
		FloatControl volume = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
		volume.setValue(gainDB);
	}

	@Override
	public String getPath() {
		return location.toString();
	}
	
	public String toString() {
		return "WavSound(" + location + ")";
	}
	
	public void addLineListener(LineListener ll) {
		clip.addLineListener(ll);
	}
}
