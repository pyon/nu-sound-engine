import java.util.Scanner;

import sounds.Sound;
import sounds.SoundEngine;
import sounds.WavSound;


public class Test extends Thread {
	public static void main(String[] args) {
		new Test().start();
	}
	
	public void run() {
//		SoundEngine.playMusic("SoundMeni.ogg");
		SoundEngine.playMusic("end.wav");
		while (true) {
			try {
				SoundEngine.playSfx("SoundMeni.ogg");
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void old() {
//		Sound sound = new OggSound("SoundMeni.ogg");
//		Sound sound = new OggSound("Groove Grove.ogg");
		Sound sound = new WavSound("end.wav");
		sound.play();
		Scanner in = new Scanner(System.in);
		
		while (true) {
			System.out.print(">");
			String input = in.nextLine();
			String[] args = input.split(" ");

			if (input.equals("play"))
				sound.play();
			else if (input.equals("stop"))
				sound.stop();
			else if (input.equals("loop"))
				sound.loop();
			else if (input.equals("pause"))
				sound.pause();
			else if (input.equals("resume"))
				sound.resume();
			else if (input.equals("close"))
				sound.close();
			else if (input.startsWith("loop"))
				sound.loop(Integer.parseInt(args[1]));
			else if (input.startsWith("volume"))
				sound.setVolume(Float.parseFloat(args[1]));
			else if (input.equals("getpath"))
				System.out.println(sound.getPath());
			else if (input.equals("isstopped"))
				System.out.println(sound.isStopped());
			else if (input.equals("isPaused"))
				System.out.println(sound.isPaused());
			else
				System.out.println("No command found");
		}
	}
}
